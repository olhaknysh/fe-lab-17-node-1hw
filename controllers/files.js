const fs = require('fs')
const path = require('path')
const {
  getFileByNameError,
  createFileError,
  DeleteError,
  UpdateError,
  ExtenstionError,
  FileAlreadyExistsError,
  NoSuchFileError,
  NoContentError,
  NoFilenameError,
} = require('../helpers/errors')

const filesPath = path.join(__dirname + '/../api/files')

const getAllFiles = async (req, res) => {
  fs.readdir(filesPath, (err, items) => {
    if (err) {
      return res.status(400).json({ message: 'Client error' })
    }
    const result = items.filter((item) => item !== '.gitkeep')
    res.status(200).json({ message: 'Success', files: result })
  })
}

const getFilesByName = async (req, res) => {
  const { filename } = req.params

  if (!filename) {
    throw new NoFilenameError('Please enter name of the file')
  }

  try {
    const data = fs.readFileSync(`${filesPath}/${filename}`, 'utf8')
    if (!data) {
      throw new Error(`No file with ${filename} filename found`)
    }

    const extension = getExtension(filename)
    const { birthtime } = fs.statSync(`${filesPath}/${filename}`)
    res.status(200).json({
      message: 'Success',
      filename,
      content: JSON.parse(data).content,
      extension,
      uploadedDate: birthtime,
    })
  } catch (err) {
    throw new getFileByNameError(err.message)
  }
}

const addFile = async (req, res) => {
  const { filename, content } = req.body
  fileValidation({ filename, content })

  if (fs.existsSync(`${filesPath}/${filename}`)) {
    throw new FileAlreadyExistsError(`${filename} already exists`)
  } else {
    const fileContent = { content }
    fs.writeFile(
      `${filesPath}/${filename}`,
      JSON.stringify(fileContent),
      function writeJSON(err) {
        if (err) {
          throw new createFileError(err.message)
        }
        res.status(200).send({ message: 'File created successfully' })
      }
    )
  }
}

const updateFile = async (req, res) => {
  const { content } = req.body
  const { filename } = req.params
  fileValidation({ filename, content })

  if (fs.existsSync(`${filesPath}/${filename}`)) {
    const data = fs.readFileSync(`${filesPath}/${filename}`, 'utf8')

    const fileContent = { content }
    fs.writeFile(
      `${filesPath}/${filename}`,
      JSON.stringify(fileContent),
      function writeJSON(err) {
        if (err) {
          throw new UpdateError(err.message)
        }
        res.status(200).json({ message: 'Success' })
      }
    )
  } else {
    throw new NoSuchFileError(`No file with ${filename} filename found`)
  }
}

const removeFile = async (req, res) => {
  const { filename } = req.params

  if (!filename) {
    throw new NoFilenameError('Please enter name of the file')
  }

  if (fs.existsSync(`${filesPath}/${filename}`)) {
    try {
      const data = fs.readFileSync(`${filesPath}/${filename}`, 'utf8')

      fs.unlinkSync(`${filesPath}/${filename}`)
      res.status(200).json({ message: 'Success' })
    } catch (err) {
      throw new DeleteError(err.message)
    }
  } else {
    throw new NoSuchFileError(`No file with ${filename} filename found`)
  }
}

function fileValidation({ filename, password, newPassword, content }) {
  const extensionRegExp = /^(js|log|txt|yaml|xml|json)$/

  if (!filename) {
    throw new NoFilenameError('Please enter name of the file')
  }
  const extenstion = getExtension(filename)

  if (!extensionRegExp.test(extenstion)) {
    throw new ExtenstionError(`${extenstion} extenstion is not supported`)
  }

  if (!content) {
    throw new NoContentError('Please specify "content" parameter')
  }
}

function getExtension(filename) {
  let end = false
  return filename
    .split('')
    .reduceRight((acc, item) => {
      if (!end) {
        acc.push(item)
      }
      if (item === '.') {
        end = true
      }
      return acc
    }, [])
    .reverse()
    .slice(1)
    .join('')
}

module.exports = {
  getAllFiles,
  getFilesByName,
  addFile,
  removeFile,
  updateFile,
}
