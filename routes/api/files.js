const express = require('express')
const router = express.Router()

const { asyncWrapper } = require('../../helpers/apiHelpers')

const {
  getAllFiles,
  getFilesByName,
  removeFile,
  addFile,
  updateFile,
} = require('../../controllers/files')

router.get('/files', asyncWrapper(getAllFiles))
router.post('/files', asyncWrapper(addFile))
router.get('/files/:filename', asyncWrapper(getFilesByName))
router.put('/files/:filename', asyncWrapper(updateFile))
router.delete('/files/:filename', asyncWrapper(removeFile))

module.exports = router
