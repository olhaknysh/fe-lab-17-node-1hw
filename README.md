# File API

**_Magage your files_**

## Overview

This API will help you with:

- getting list of files
- getting certain file by name
- adding file
- changing file
- deleting file

## Getting started

### Commom routes

`get 'api/files/'`

Returns list of all files

`get 'api/files/:filename'`

Returns specific file by filename

Url query password is necessary in order to get access

`post 'api/files/'`

Adds new file

Field password is necessary to encrypt your file

`put 'api/files/:filename'`

Changes contact

Url query password is necessary in order to get access

`delete 'api/files/:filename`

Deletes specific file

Url query password is necessary in order to get access
