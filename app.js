const express = require('express')
const cors = require('cors')
const fs = require('fs')
const path = require('path')
const morgan = require('morgan')

const filesRoutes = require('./routes/api/files')
const { errorHandler, notFoundHandler } = require('./helpers/apiHelpers.js')

const app = express()
const accessLogStream = fs.createWriteStream(
  path.join(__dirname, 'access.log'),
  {
    flags: 'a',
  }
)

app.use(morgan('combined', { stream: accessLogStream }))
app.use(cors())
app.use(express.json())

app.use('/api', filesRoutes)

app.use(notFoundHandler)
app.use(errorHandler)

module.exports = app
