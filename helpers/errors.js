class CustomError extends Error {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class ExtenstionError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class FileAlreadyExistsError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class NoSuchFileError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class DeleteError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class UpdateError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class NoContentError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class getFileByNameError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class NoFilenameError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class createFileError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

module.exports = {
  CustomError,
  createFileError,
  getFileByNameError,
  DeleteError,
  UpdateError,
  ExtenstionError,
  FileAlreadyExistsError,
  NoSuchFileError,
  NoContentError,
  NoFilenameError,
}
